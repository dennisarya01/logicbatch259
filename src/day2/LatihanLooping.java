package day2;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Scanner;

public class LatihanLooping {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);		
		System.out.println("Masukkan nilai n = ");
		
		int n = input.nextInt();
		
		int x = 1;
		
		// array dari integer
		int[] tempArray = new int[n];
		
		// untuk memasukan data kedalam array & mencetaknya
		for (int i = 0; i < tempArray.length; i++) {
			tempArray[i] = x;
			x++;
			System.out.print(tempArray[i] + " ");
		}
		
		System.out.println("Hello World");
		// untuk mencetak data dari temporary array
		for (int i = 0; i < tempArray.length; i++) {
			System.out.print(tempArray[i] + " ");
		}
		System.out.println(Arrays.toString(tempArray));
	}

}


//for (int i = 0; i < n; i++) {
//x++;
//System.out.print(x + " ");
//x+=1;
//if (i % 2 == 0) {
//	System.out.print(i+ " ");
//}
//}